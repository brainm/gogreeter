package gogreeter

import (
	"bitbucket.org/brainm/gogreeter/china"
	"bitbucket.org/brainm/gogreeter/spain"
)

func Hello(lang, name string) string {
	switch lang {
	case "cn":
		return china.Hello(name)
	case "es":
		return spain.Hello(name)
	default:
		return "Kengury"
	}
}
